# 前言：
### 全部内容分为三部分：
1. 简单 安装 下载 使用三步
2. 单独开启某项功能★★★ 
3. 讲解备份与恢复命令教程♥♥♥

### 功能实现：
##### 1.自定义备份模式
1. 每天备份一次周末做下周的全量备份, 周一到周六做周末的全量备份
2. 每天备份一次周末做下周的全量备份, 周一到周六每天做上一天的增量备份
3. 每天第一次备份为全量备份之后的备份为全量备份的增量备份
* `ps：`备份前检测全量备份是否存在 如果不存在自动创建全量备份

##### 2.可备份到远程服务器 
##### 3.可备份到FTP服务器 
##### 4.备份失败邮件提醒
##### 5.备份失败阿里短信提醒
`ps：`2-5功能可单独设置也可组合设置


### 脚本说明：

```
/script/innobackup.sh [OPTION]

    --help, -h             查看帮助信息
    --data-dir, -d         数据备份存储目录
    --mode, -m             备份模式 [1|2|3] 必选
      备份模式: 1.周日做全量备份 周一到周六每天做上周日的增量备份
      备份模式: 2.周日做全量备份 周一到周六每天做上一天的增量备份
      备份模式: 3.循环 每天第一次做全量备份之后的备份做基于当天的全量备份的增量备份
    --alisms               备份失败短信告警 启用: --alisms true 默认禁用
    --mail                 备份失败邮件告警 启用: --mail true 默认禁用
    --mail-addr            邮件告警邮箱地址
      如果启用邮件告警: 请手动测试邮件告警脚本确定能正常发送邮件

    --remoute-bak          远程备份 启用: --remoute-bak true 默认禁用
    --remoute-server       远程服务器地址： root@192.168.8.168
    --remoute-dir          远程服务器路径： /home/data
      如果启用远程备份: 1.配置本机到远程服务器的秘钥登录 2.登录远程服务器创建备份路径
      
    --ftp-bak          FTP备份 启用: --remoute-bak true 默认禁用
    --ftp-server       FTP服务器地址： 192.168.8.168
    --ftp-dir          FTP服务器路径： / 默认跟目录
    --ftp-username     FTP服务器用户名： user
    --ftp-password     FTP服务器密码： password
      如果启用FTP备份: 1.确保本机装有ftp 2.本机能正确连上FTP服务器 3.FTP服务器备份路径存在

    示例:
    	#1. 使用备份模式1备份路径为 /home/backup
        /script/innobackup.sh --mode 1 --data-dir /home/backup
		
		#2. 使用备份模式1使用脚本中默认备份，启用备份失败邮件与短信告警
        /script/innobackup.sh --mode 1 --mail true --mail-addr example@domain.com --alisms -true
		
		#3. 使用备份模式1启用远程备份(备份完成后压缩并使用rsync将压缩文件上传到远程服务器指定目录)
        /script/innobackup.sh --mode 1 --remoute-bak true --remoute-server root@192.168.8.71 --remoute-dir /home/backupmysql
		
        #4. 使用备份模式1启用FTP备份(备份完成后压缩并使用FTP将压缩文件上传到远程服务器指定目录)
        /script/innobackup.sh --mode 1 --ftp-bak true --ftp-server 192.168.8.71 ftp-username ganchu --ftp-password 123456 --ftp-dir /
		
        #5.组合全家福
        /script/innobackup.sh --mode 1 --alisms true --mail true --mail-addr example@domain.com --remoute-bak true --remoute-server ubuntu@116.62.18.81 --remoute-dir /home/ubuntu/backupmysql --ftp-bak true --ftp-server 192.168.8.71 --ftp-username ganchu --ftp-password 123456

```

## 1.安装Percona-XtraBackup教程：

执行：
```
wget https://downloads.percona.com/downloads/Percona-XtraBackup-2.4/Percona-XtraBackup-2.4.24/binary/debian/focal/x86_64/percona-xtrabackup-24_2.4.24-1.focal_amd64.deb
sudo apt-get update
sudo apt-get install libdbd-mysql-perl
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libev4
sudo dpkg -i percona-xtrabackup-24_2.4.24-1.focal_amd64.deb
```

参考资料：

文档：https://www.percona.com/doc/percona-xtrabackup/8.0/installation/apt_repo.html#

下载：https://www.percona.com/downloads/Percona-XtraBackup-2.4/LATEST/

## 2.下载全量/增量脚本

```
mkdir /script && cd $_
curl -O https://gitee.com/ganchu/mysqlbackup/raw/master/alisms.php
curl -O https://gitee.com/ganchu/mysqlbackup/raw/master/innobackup.sh
chmod +x innobackup.sh alisms.php
```


命令替换脚本里的mysql密码或者自行修改innobackup.sh

```
password=162dfdgdsgg
sed -i "s/--password=.*'/--password=\'$password\'/" innobackup.sh
```


## 3.使用教程

先执行脚本看是否可用（也可用后面的备份命令测试软件是否可用）

```
sudo /script/innobackup.sh --mode 1 --data-dir /home/backup
ls /home/backup
```


设置定时任务

```
sudo crontab -e
```

例子1：每天凌晨 1:10 分执行备份 备份策略: 周日做全量备份 周一到周六每天做上周日的增量备份 本地备份

```
10 1 * * * /script/innobackup.sh --mode 1 --data-dir /home/backup
```

例子2：每天凌晨零点做昨天的全量备份 备份策略: 每半小时做基于全量备份的增量备份 启用FTP远程备份 启用远程备份 启用失败邮件告警 启用短信告警

```
*/30 * * * * --mode 3 --alisms true --mail true --mail-addr example@domain.com --remoute-bak true --remoute-server ubuntu@116.62.18.81 --remoute-dir /home/ubuntu/backupmysql --ftp-bak true --ftp-server 192.168.8.71 --ftp-username ganchu --ftp-password 123456
```
`ps`:远端文件夹需要提前新增，注意home下面用户名为ssh登录用户名


## ★★★启用邮件提醒教程
检测是否已安装

```
sendEmail
```

安装：

```
sudo apt install sendemail
```

修改配置：send_email()里面的邮箱与密码与smtp地址

## ★★★启用短信提醒教程
确保已安装php

```
php -v
```

安装：

```
sudo apt-get install php
```

修改`alisms.php`文件第三行的配置
配置分别是：手机号码，短信模板号，模板参数，短信签名，阿里短信AccessKeyId，阿里短信accessKeySecret


## ★★★启用FTP备份教程
检测是否已安装

```
ftp
```

安装：

```
sudo apt-get install ftp
```


## ★★★启用远程备份教程
#### 备份端服务器：

查看本机密钥

```
cat ~/.ssh/id_rsa.pub
```

因为脚本用到sudo，所以用的是root账户远程，需要复制本地的秘钥到root文件夹

```
sudo cp ~/.ssh/.id_rsa /root/.ssh
```

测试下是否可以免密ssh

```
sudo ssh ubuntu@192.168.8.168
```


#### 接收备份文件服务器：

添加备份端服务器的公钥到授权文件

```
vim ~/.ssh/authorized_keys
```





# 1.♥♥♥备份命令教程

全量备份命令

```
sudo innobackupex --defaults-file=/etc/my.cnf --user=root --password='162d333e2d2fda83' --databases='smple_admin_com' --no-timestamp /home/backup &>>/home/backup/a.log
```

增量备份命令（需要先全量备份）

```
sudo innobackupex --defaults-file=/etc/my.cnf --user=root --password='162d333e2d2fda83' --databases='smple_admin_com' --incremental --incremental-basedir=/home/backup --no-timestamp /home/backupinc &>>/home/backupinc/a.log
```

ps:文件夹与文件要先创建喔！

# 2.♥♥♥恢复教程
#### 2.1恢复全量
准备全量数据

```
sudo innobackupex --apply-log-only /home/backup
```

关闭mysql服务

```
service mysqld stop
```

恢复数据（恢复前确保/www/server/data目录已删除）

```
sudo innobackupex --copy-back /home/backup
```

#### 2.2恢复增量数据
合并增量数据到全量（如果有多个增量则按顺序执行）

```
sudo innobackupex --apply-log-only /home/backup --incremental-dir=/home/backupinc1
```

`ps`:合并完在/home/backup下会新建一个时间文件夹如：2021-10-15_01-35-07

关闭mysql服务

```
service mysqld stop
```

恢复数据（恢复前确保/www/server/data目录已删除）

```
sudo innobackupex --copy-back /home/backup/2021-10-15_01-35-07
```

